'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')


/* Api Routes */

Route.group(() => {
    Route.post("register", "UserController.register");//.formats(['json']);
    Route.post("login", "UserController.login");
}).prefix('api').middleware("guest")

Route.group(() => {
    Route.get("user", "UserController.user");

}).prefix('api').middleware("auth")


///???
//Route.on('/').render('welcome')
