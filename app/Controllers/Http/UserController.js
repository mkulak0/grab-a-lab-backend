'use strict'

const User = use('App/Models/User');
const Hash = use('Hash');

class UserController {
    async register({request, response, auth}){
        const { email, password, username } = request.all();
        const user = new User();

        user.username = username;
        user.email = email;
        user.password = password;

        await user.save();

        return JSON.stringify(user);
    }

    async login ({ auth, request }) {
        /* const { email, password } = request.all()
        await auth.attempt(email, password)
    
        return 'Logged in successfully' */
        const { email, password } = request.all();
        //const user = await User.query().where('email', '=', email).;
        const user = await User.findBy('email', email);

        const isVerified = await Hash.verify(password, user.password)

        if(isVerified){
            const token = await auth.generate(user);
            
            return JSON.stringify(token);
        }
        return isVerified;
    }

    async user({auth, request}){
        try {
            return await auth.getUser();
        } catch (error) {
            return false;
        }
    }
}

module.exports = UserController
